<?php
require_once('inc/config.php');
require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  <head>
		<?php require_once('templates/head.php'); ?>
  </head>
  <body>
		<div class="off-canvas-wrapper">
			<?php require_once('templates/header.php');	?>
			<main class="container off-canvas-content" data-off-canvas-content>
				<div class="row">
					<h1 class="page-title">About</h1>

					<h2 class="title">Contexte</h2>

					<p>Cette application web a été développée durant le deuxième semestre de ma première année à l'école d'ingénieure dans le cadre du cours d'Icommunication.</p>
					<h2 class="title">Remarques</h2>

					<p>Des fonctionnalités de tris et de filtres étaient initialement prévues. Par manque de temps et de compétences je ne m'y suis pas attelé. Je rajouterai cela ultérieurement.</p>
				</div>
			</main>
		</div>
		<?php require_once('templates/footer.php'); ?>
  </body>
</html>