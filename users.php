<?php
require_once('inc/config.php');
require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  <head>
		<?php require_once('templates/head.php'); ?>
  </head>
  <body>
		<div class="off-canvas-wrapper">
			<?php require_once('templates/header.php');	?>

			<main class="container off-canvas-content" data-off-canvas-content>
				<div class="row">
					<h1 class="page-title">Liste des utilisateurs</h1>
					<ul class="tasklist">
						<li class="tasklist-header">
							<span class="tasklist-item-id">
								ID
							</span>
							<span class="tasklist-item-priority">
								Utilisateur
							</span>
							<span class="tasklist-item-description">
								Email
							</span>
							<span class="tasklist-item-actions">
								Actions
							</span>
						</li>
						<?php
	          $query = $db -> query('SELECT * FROM user');
	          while($data = $query -> fetch()):
	          ?>
						<li class="tasklist-item">
	            <span class="tasklist-item-id">
	              <?php echo $data['id']; ?>
	            </span>
	            <span class="tasklist-item-priority">
	              <?php echo $data['username']; ?>
	            </span>
							<span class="tasklist-item-description">
	              <?php echo $data['email']; ?>
	            </span>
	            <span class="tasklist-item-actions">
								<a href="edituser.php?id=<?php echo $data['id']; ?>">
	                <i class="fa fa-pencil" aria-hidden="true"></i>
	              </a>
	              <a href="deleteuser.php?id=<?php echo $data['id']; ?>">
	                <i class="fa fa-window-close-o" aria-hidden="true"></i>
	              </a>
	            </span>
	          </li>
	          <?php endwhile; ?>
					</ul>
				</div>
			</main>
		</div>
		<?php require_once('templates/footer.php');	?>
  </body>
</html>
