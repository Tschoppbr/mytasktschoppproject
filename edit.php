<?php
require_once('inc/config.php');
require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  <head>
		<?php require_once('templates/head.php'); ?>
  </head>
  <body>
		<div class="off-canvas-wrapper">
			<?php require_once('templates/header.php'); ?>

			<main class="container off-canvas-content" data-off-canvas-content>
				<div class="row">
					<h1 class="page-title">Editer une tâche</h1>
					<?php
					$query = $db -> prepare('SELECT * FROM task WHERE id = ?');
	        $query -> execute(array($_GET['id']));
	        $data = $query -> fetch();
					?>
					<form method="post" action="update.php" class="small-12 medium-6 collumn">
						<input name="id" type="hidden" value="<?php echo $_GET['id']; ?>"/>
            		<label class="text">Description</label>
            			<textarea class="textfield" name="description" rows="6"><?php echo $data['description']; ?></textarea>
            		<label class="text">Priorité</label>
            			<select class="textfield" name="priority">
							<option value="<?php echo $data['priority']; ?>"><?php echo $data['priority']; ?></option>
              				<?php for($i = 1; $i <= 5; $i++): ?>
                			<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
              				<?php endfor; ?>
            			</select>
            		<label class="text">Délai</label>
            			<input class="textfield" type="date" name="due_at" value="<?php echo $data['due_at']; ?>"/>
					<label class="text">Assignée à</label>
						<select class="textfield" name="assigned_to">
							<?php
							$query = $db -> query('SELECT * FROM user');
							while($data =	$query -> fetch()):
							?>
								<option value="<?php echo $data['id']; ?>"><?php echo $data['username']; ?></option>
							<?php
							endwhile;
							?>
            </select>
            <input type="submit" value="Modifier" class="button"/>
	        </form>
				</div>
							<?php require_once('templates/footer.php'); ?>
			</main>
		</div>
  </body>
</html>
