<?php
require_once('inc/config.php');
require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  <head>
		<?php require_once('templates/head.php'); ?>
  </head>
  <body>
		<div class="off-canvas-wrapper">
			<?php require_once('templates/header.php'); ?>

			<main class="container off-canvas-content" data-off-canvas-content>
				<div class="row">
					<h1 class="page-title">Editer un utilisateur</h1>
					<?php
					$query = $db -> prepare('SELECT * FROM user WHERE id = ?');
	        		$query -> execute(array($_GET['id']));
	        		$data = $query -> fetch();?>

					<form method="post" action="edituser-action.php" class="small-12 medium-6 collumn">
						<input name="id" type="hidden" value="<?php echo $_GET['id']; ?>"/>
            			<label class="text">Nom</label>
            			<input class="textfield" type="text" name="username" value="<?php echo $data['username']; ?>"/>
						<label class="text">Adresse e-mail</label>
            			<input class="textfield" type="email" name="email" value="<?php echo $data['email']; ?>"/>
						<label class="text">Mot de passe</label>
            			<input class="textfield" type="password" name="password" value="<?php echo $data['password']; ?>"/>
            			<input type="submit" value="Modifier" class="button"/>
	        		</form>
				</div>
			</main>

			<?php require_once('templates/footer.php'); ?>
		</div>
  </body>
</html>
