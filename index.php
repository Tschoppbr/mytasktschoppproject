<?php
require_once('inc/config.php');
require_once('inc/security.php');
?>
<!doctype html>
<html class="no-js" lang="fr">
  <head>
		<?php require_once('templates/head.php'); ?>
  </head>
  <body>
		<div class="off-canvas-wrapper">
			<?php require_once('templates/header.php'); ?>

			<main class="container off-canvas-content" data-off-canvas-content>
				<div class="row">
					<h1 class="page-title">Tâches</h1>
					<ul class="tasklist">
						<li class="tasklist-header">
							<span class="tasklist-item-id">
								ID
							</span>
							<span class="tasklist-item-priority">
								Priorité
							</span>
							<span class="tasklist-item-description">
								Description
							</span>
							<span class="tasklist-item-creator">
								Créé par
							</span>
							<span class="tasklist-item-assigned">
								Assigné à
							</span>
							<span class="tasklist-item-due">
								Date d'échéance
							</span>
							<span class="tasklist-item-actions">
								Actions
							</span>
						</li>
						<?php
	          $query = $db -> prepare('SELECT
																		task.id,
																		description,
																		created_at,
																		due_at,
																		priority,
																		status,
																		creator.id as creator_id,
																		creator.username as creator_name,
																		assignee.id as assignee_id,
																		assignee.username as assignee_name
																		FROM task
																		INNER JOIN user as creator on created_by = creator.id
																		LEFT JOIN user as finishor on done_by = finishor.id
																		INNER JOIN user as assignee on assigned_to = assignee.id');

						$query -> execute();
	          	while($data = $query -> fetch()):
	          	?>
					<li class="tasklist-item<?php if($data['status'] == 'close'): ?> tasklist-item-close<?php endif; ?>">
	            <span class="tasklist-item-id">
	              	<?php echo $data['id']; ?>
	            </span>
				<span class="tasklist-item-priority">
	              	<?php echo $data['priority']; ?>
	            </span>
	            <span class="tasklist-item-description">
	              	<?php echo $data['description']; ?>
	            </span>
				<span class="tasklist-item-creator">
					<?php echo $data['creator_name']; ?>
				</span>
				<span class="tasklist-item-assigned">
					<?php echo $data['assignee_name']; ?>
				</span>
	            <span class="tasklist-item-due">
	              <?php echo $data['due_at']; ?>
	            </span>
	            <span class="tasklist-item-actions">
						<a href="done.php?id=<?php echo $data['id']; ?>">
	                <i class="fa fa-check-square-o" aria-hidden="true"></i>
	              		</a>
	              		<a href="edit.php?id=<?php echo $data['id']; ?>">
	                <i class="fa fa-pencil" aria-hidden="true"></i>
	              		</a>
	              		<a href="delete.php?id=<?php echo $data['id']; ?>">
	                <i class="fa fa-window-close-o" aria-hidden="true"></i>
	              		</a>
	            </span>
	          </li>
	          <?php endwhile; ?>
					</ul>
				</div>

			</main>			
		</div>
		<?php require('templates/footer.php'); ?>
  </body>
</html>
