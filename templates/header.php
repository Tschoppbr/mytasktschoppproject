<?php
require_once('inc/config.php');
require_once('inc/security.php');
?>
<header class="top row expanded">
	<div class="top-bar">
		<div class="top-bar-title">
			<button class="menu-icon hide-for-large" type="button" data-toggle="offCanvas"></button>
			<a href="index.php">Mon gestionnaire de tâches</a>
		</div>
		<div class="top-bar-right">
			<?php
			$query =	$db -> prepare('SELECT * FROM user WHERE id = ?');
			$query -> execute(array($_SESSION['userid']));
			$data = $query -> fetch();
			?>
			<ul class="dropdown menu" data-dropdown-menu>
				<li>
					<img src="assets/img/<?php echo $_SESSION['userid']; ?>.jpg" class="header-picture" />
					<ul class="menu">
						<li><a href="edituser.php?id=<?php echo $_SESSION['userid']; ?>"><?php echo $data['username']; ?></a></li>
						<li><a href="logout.php">Déconnexion</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
</header>

<nav class="nav off-canvas-absolute position-left reveal-for-large" id="offCanvas" data-off-canvas>
	<ul class="vertical menu">
		<li><a href="add.php">Ajouter une tâche</a></li>
		<li><a href="users.php">Liste des utilisateurs</a></li>
		<li><a href="adduser.php">Ajouter un utilisateur</a></li>
		<li><a href="assets/guide.pdf">Guide de l'utilisateur</a></li>
		<li><a href="assets/tech.pdf">Manuel technique</a></li>
		<li><a href="about.php">About</a></li>
	</ul>
</nav>
